database_url = 'bolt://neo4j:graph.db@localhost:7687'

load_link_sql = 'sql/load_link.sql'
drop_partition_sql = 'sql/drop_partition.sql'
load_top_level_domains_sql = 'sql/load_top_level_domains.sql'
