match (t:TopLevelDomain)
match (d:Domain) where d.isParent = 1 and d.name ends with t.name
create (d)-[:OF_TOP_LEVEL_DOMAIN]->(t)