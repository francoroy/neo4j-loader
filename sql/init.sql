CREATE INDEX ON :Domain(partition_id)
CREATE INDEX ON :Domain(name)
CREATE INDEX ON :Domain(name, partition_id)

CREATE INDEX ON :Domain(country)

LOAD CSV FROM 'data/top-level-domain-names.csv' AS line
CREATE (:TopLevelDomain { name: line[1], type: line[2], description: line[3]})
-- Domain,Type,Sponsoring Organisation
