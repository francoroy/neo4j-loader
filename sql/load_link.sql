MERGE (source:Domain {name: "{{source}}", partition_id: {{partition_id}} })
  ON CREATE
      SET source.created = timestamp()
      SET source.country = "{{src_country}}"
MERGE (sParent:Domain {name: "{{source_domain}}", isParent: true})
MERGE (source)-[spLink:SUBDOMAIN_OF]->(sParent)

MERGE (dest:Domain {name: "{{destination}}", partition_id: {{partition_id}} })
  ON CREATE
      SET dest.created = timestamp()
      SET dest.country = "{{dst_country}}"

MERGE (dParent:Domain {name: "{{destination_domain}}", isParent: true})
MERGE (dest)-[dpLink:SUBDOMAIN_OF]->(dParent)

MERGE (source) -[l:LINKS_TO]-> (dest)
  ON MATCH
      SET l.numLinks = l.numLinks + {{num_links}}
  ON CREATE
      SET l.created = timestamp()
      SET l.numLinks = {{num_links}}