-- top sources by country
match (s)-[l1:LINKS_TO]->(d:Domain)
match (s)-[:SUBDOMAIN_OF]->(p:Domain)
match (p)-[:OF_TOP_LEVEL_DOMAIN]->(tld) where tld.type = 'country-code'
return (p)-[:OF_TOP_LEVEL_DOMAIN]->(tld), sum(l1.numLinks) as outgoing_links
order by outgoing_links desc limit 100

-- top destinations by country
match (s)-[l1:LINKS_TO]->(d:Domain)
match (d)-[:SUBDOMAIN_OF]->(p:Domain)
match (p)-[:OF_TOP_LEVEL_DOMAIN]->(tld) where tld.type = 'country-code'
return (p)-[:OF_TOP_LEVEL_DOMAIN]->(tld), sum(l1.numLinks) as outgoing_links
order by outgoing_links desc limit 100

-- instagram sources in Japan
match (s)-[l1:LINKS_TO]->(d:Domain) where not d.name ends with "google.com"
match (s)-[:SUBDOMAIN_OF]->(p:Domain)
match (p)-[:OF_TOP_LEVEL_DOMAIN]->(tld) where tld.type = 'country-code' and tld.name = '.jp'
return (p)-[:OF_TOP_LEVEL_DOMAIN]->(tld), sum(l1.numLinks) as outgoing_links
order by outgoing_links desc limit 100