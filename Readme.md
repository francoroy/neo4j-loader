# Neo4j Loader

## Configuration

#### Neo4j Configuration
Set the neo4j database_url in config.py

Install neo2py
```bash
pip install -r requirements.txt
```

## Running the loader

```bash
loader.py partition_key --path path1 [--path path2 ...]
```

Run the loader with data files or with directories that contains data files that starts with "part"

For example, to load the files `data/part0/part*`, with partition key 201712 run

```bash
loader.py 201712 --path data/part0
```

To run with a single file 

```bash
loader.py 201712 ---path path_to_file
```
\* Note to export PYTHONPATH = $PWD

## The Data

### Download the data

You can download the data from the s3 bucket `s3://idc-output-emr/links_extractor/`

The credentials for accessing the data can be found in `docs/aws-keys`

Add these keys to download using aws cli
```bash
cat docs/aws-keys.ini >> ~/.aws/credentials
aws s3 sync s3://idc-output-emr/links_extractor/ data --profile idc-bd
```

### Data Format

The loader expects that each row in the data files would be in the following tsv format:
```
"src:string;dst:string" num_links:int
```
For example, if two links were found from 007700.blog.shinobi.jp to www.blogmura.com then the corresponding row is
```
"007700.blog.shinobi.jp;www.blogmura.com"	2
```

### Loading a link
for each row in the data file, two nodes with the specific partition id would be created or merged, 
and a relationship with the property numLinks=2 would be created if it does not already exist, or merged by aggregating
numLinks during the load of a partition to create only one relationship for the same src->dst in a given partition

That way an entire partition can be easily dropped when re-loading data or when deleting old data

During load, Domain nodes for subdomains are also created if they does not already exist, so we can group subdomains by their domain

Indices create statements can be found in `sql/init.sql`

## Querying the data

### Top Destinations

#### Count Incoming Links
```sql
match (s:Domain)-[l1:LINKS_TO]->(d:Domain) 
return d.name, count(*) as num_sources 
order by num_sources desc limit 20
```
#### Sum Incoming Links
```sql
match (s:Domain)-[l1:LINKS_TO]->(d:Domain) 
return d.name, sum(l1.numLinks) as incoming_links 
order by incoming_links desc limit 20
```

### Top Sources

#### Count Outgoing Links
```sql
match (s:Domain)-[l1:LINKS_TO]->(d:Domain) 
return s.name, count(*) as num_destinations 
order by num_destinations desc limit 20
```

#### Sum Outgoing Links
```sql
match (s:Domain)-[l1:LINKS_TO]->(d:Domain) 
return s.name, sum(l1.numLinks) as outgoing_links 
order by outgoing_links desc limit 20
```

## Top Level Domains
Top level domains data can be found at `data/top-level-domain-names.csv` and was loaded once using `load csv` command that can be found at `sql/load_top_level_domains.sql` 

Following each load of a partition, links are created between Domain nodes that are parents (i.e not subdomains), and TopLevelDomain nodes.

This let us find top sources/destinations per grouped by top level domains.

For Example, to find top sources by country run
```sql
match (s)-[l1:LINKS_TO]->(d:Domain)
match (s)-[:SUBDOMAIN_OF]->(p:Domain)
match (p)-[:OF_TOP_LEVEL_DOMAIN]->(tld) where tld.type = 'country-code'
return (p)-[:OF_TOP_LEVEL_DOMAIN]->(tld), sum(l1.numLinks) as outgoing_links
order by outgoing_links desc limit 100
```

Additional queries can be found in `sql/queries.sql`

Output images can be found at `docs/*.png`
