import argparse
import glob
import re

import sys

import time

import os

from jinja2 import Template
from py2neo import Graph
import config

class Neo4jLinksLoader():
    def __init__(self, database_url, partition_id=None, drop=False):
        print "Connecting to neo4j %s" % database_url
        self.graph = Graph(database_url)
        print "Connected to neo4j"

        self.partition_id = partition_id
        with open(config.load_link_sql, 'r') as myfile:
            self.load_link_tpl = Template(myfile.read())

        if partition_id and drop:
            with open(config.drop_partition_sql, 'r') as myfile:
                stmt = Template(myfile.read()).render({'partition_id': partition_id})
                print "Dropping Partition %s" % partition_id
                tx = self.graph.begin()
                tx.append(stmt)
                tx.commit()

    def loadRow(self, row):
        data = self.parseRow(row)
        if data is not None:
            source, destination, count = data
            self.loadLink(source, destination, count)

    def parseRow(self, row):
        # "2ch-mato.site;coinmatomesokuho.blog.jp"	1
        match = re.match('"([^;]+);(.*)"\t(\d+)\n?', row)
        if not match:
            return None

        source = match.group(1)
        dest = match.group(2)
        count = match.group(3)
        return source, dest, count

    def getDomain(self, hostname):
        h_split = hostname.split(".")
        if len(h_split) > 2:
            return ".".join(h_split[1:])
        else:
            return hostname

    def loadLink(self, source, destination, count):

        stmt_params = {
            'source': source,
            'source_domain': self.getDomain(source),
            'destination': destination,
            'destination_domain': self.getDomain(destination),
            'num_links': count,
            'partition_id': self.partition_id
        }
        stmt = self.load_link_tpl.render(stmt_params)

        tx = self.graph.begin()
        tx.append(stmt)
        tx.commit()

    def loadFile(self, input_file):
        print "Loading raw input file %s" % input_file
        start = time.time()
        with open(input_file, 'r') as input_file:
            line = 1
            while True:
                row = input_file.readline()
                if not row:
                    break

                self.loadRow(row)  # todo: load in parallel
                if line % 1000 == 0:
                    print "(%ss) #%s" % (time.time() - start, line)

                line = line + 1

        end = time.time()
        print "Load Completed in %.3f minutes" % ((end-start)/60)

    def loadTopLevelDomains(self, data_file):
        print "Loading top level domains from %s" % data_file

        os.environ['dbms.directories.import'] = os.path.dirname(data_file)

        with open(config.load_top_level_domains_sql, 'r') as input_file:
            stmt = Template(input_file.read()).render({
                'file_path': os.path.basename(data_file)
            })

        tx = self.graph.begin()
        tx.append(stmt)
        tx.commit()

def main():

    parser = argparse.ArgumentParser(description='Domains data loader')

    parser.add_argument('action', help='load_domains / load_top_level_domains')
    parser.add_argument('--partition_id', help='partition id (int) e.g. 201712')
    parser.add_argument('--path', dest='paths', default=[], action='append', help='input file / input directory. if a directory is given then all files that matches dir/part* will be loaded')
    parser.add_argument('--drop_partition', action='store_true', default=False, help='drop entire partition before loading')

    args = parser.parse_args()


    if args.action == 'load_top_level_domains':
        loader = Neo4jLinksLoader(config.database_url)
        loader.loadTopLevelDomains(args.paths[0])

    elif args.action == 'load_domains':
        loader = Neo4jLinksLoader(config.database_url, args.partition_id, drop=args.drop_partition)
        for path in args.paths:
            if not os.path.exists(path):
                print "%s does not exist" % path
                continue

            if os.path.isdir(path):
                files = glob.glob("%s/part*" % path)
            else:
                files = [path]

            for file in files:
                loader.loadFile(file)


if __name__ == "__main__":
    main()
